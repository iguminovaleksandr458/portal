"use client";
import { useRef, useEffect, useState, ReactNode } from "react";
import { createPortal } from "react-dom";
import styled from "styled-components";

const ModalSection = styled.section`
  display: flex;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 37.5rem;
  height: 18.75rem;
  background-color: #439b43;
  border-radius: 20px;
  opacity: 0.2;
`;

interface PortalProps {
  children: ReactNode;
}

export const Portal = (props: PortalProps) => {
  const ref = useRef<Element | null>(null);
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    // ref.current = document.querySelector<HTMLElement>("#portal")
    ref.current = document.getElementById("portal");
    setMounted(true);
    console.log("Mounted");
  }, []);

  return mounted && ref.current
    ? createPortal(<ModalSection>{props.children}</ModalSection>, ref.current)
    : null;
};
